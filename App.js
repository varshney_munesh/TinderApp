/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform, FlatList, PanResponder,
  StyleSheet, TouchableOpacity, Animated,
  Text, Dimensions, Easing, TouchableHighlight,
  View, ScrollView, Image, ImageBackground
} from 'react-native';
const { width, height } = Dimensions.get('window');

let i = 0
let j = 0

export default class App extends Component {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0)
    this.state = {
      arrData: ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsgKWaWvMfgSmQjJBETlectexGQ4qM_Yf4eiP44iWKUqBASfGvUA", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRp0tRJtlX7OswpSRxKj4mUKxfMxo6Nl1_z5UA4xkHjcS3kI7SCYg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR--wdwKyp4bQqRKv1DoImPR_Uaowg3aiYSd1K0luWcfIMg5xOegg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3s2FNr2pbOc7wMnYjIRYObXHmdWbwFFj1Pl0vpg4aqmHEosQipQ", "https://www.elastic.co/assets/bltada7771f270d08f6/enhanced-buzz-1492-1379411828-15.jpg"],
      swipe: null
    }
  }

  /****** Use for animation time function ******/
  animate() {
    this.animatedValue.setValue(0)
    Animated.timing(
      this.animatedValue,
      {
        toValue: this.state.swipe == 'left' ? -1.5 : 1.5,
        duration: 1000,
        easing: Easing.linear
      }
    ).start()
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,

      onPanResponderGrant: (e, gestureState) => {
        console.log("hhh " + gestureState.vx)
        if (gestureState.vx > 0) {
          this.setState({ swipe: "right" })
        } else {
          this.setState({ swipe: "left" })
        }
      },

      onPanResponderMove: Animated.event([
      ]),

      onPanResponderRelease: (e, { vx, vy }) => {
        this.animate()
        setTimeout(() => {
          this.animatedValue.setValue(0)
          let arr = this.state.arrData
          arr.pop()
          this.setState({ arrData: arr })
        }, 800)
      }
    });
  }

  /****** Create view function ******/
  makeView(i, data) {
    let length = data.length
    j = j + 5
    i++
    const marginLeft = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 260]
    })
    const rotate = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '30deg']
    })

    return (
      <Animated.View ref={i}
        {...this._panResponder.panHandlers}
        style={[i == length ? { marginLeft, transform: [{ rotate: rotate }] } : null, { height: 300 - j, width: (width - 40)}]}>
        <ImageBackground style={{ height: 300 - j, width: (width - 40)  }}
          source={{ uri: data[i - 1] }}
        >
          {i < length ? this.makeView(i++, data) : null}
        </ImageBackground>
      </Animated.View>
    )
  }

  /****** Call first time function ******/
  makeLoop(data) {
    let arr = []
    j = 0
    i = 0
    arr.push(this.makeView(i, data))
    return (
      arr
    )
  }
  render() {
    return (
      <View style={styles.container}>
        {this.state.arrData.length > 0 ? <View style={{ marginTop: 100 }}>
          {this.makeLoop(this.state.arrData)}
        </View> : <View style={styles.mainPopConView}>
            <Text style={styles.titleLbl}>All Done!</Text>
            <View style={styles.lineView}></View>
            <View style={{ width: "100%" }}>
              <Text style={styles.descLbl}>There's no more content here!</Text>
            </View>
            <Text style={styles.bottomLbl}>Get more!</Text>
          </View>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  lineView: {
    width: "90%",
    height: 1,
    backgroundColor: "silver",
    marginTop: 10,
    opacity: 0.5
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mainPopConView: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    borderColor: "silver",
    borderWidth: 1,
    height: 160,
    width: width - 40
  },
  titleLbl: {
    marginTop: 10,
    fontSize: 20,
    fontWeight: "bold",
  },
  descLbl: {
    marginTop: 10,
    fontSize: 14,
    marginLeft: 20
  },
  bottomLbl: {
    marginTop: 20,
    fontSize: 20,
    color: "blue",
  }
});
